import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv'
import postRoutes from './routes/posts.js'

const app = express();
dotenv.config()

app.use(express.json()); //Used to parse JSON bodies
app.use(express.urlencoded()); //Parse URL-encoded bodies
app.use(cors());

app.use('/posts', postRoutes);

// const CONNECTION_URL = 'mongodb+srv://pushkar.gautam1994@gmail.com:@cluster0.crjnx.gcp.mongodb.net/memories-app-db?retryWrites=true&w=majority'
// const CONNECTION_URL = 'mongodb://127.0.0.1:27017/memories_db'
const PORT = process.env.PORT || 5000

//using moongoose to connect to database
mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
    .catch((error) => console.log(error.message))

mongoose.set('useFindAndModify', false);



